# Forge Confluence App

This project contains a Forge app written in Javascript for a TODO app on the right panel of the Confluence homepage.

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

### Forge Challenges as of version 0.25.0

1. Debugging, Source Map doesn’t sufficiently trace errors to the developers end.

2. **UseEffect** hook is not working.

3. Forge UI Checkbox component doesn’t emit change
   event. It needs to be decoupled from Form.

4. Forge UI form component doesn’t have
   a way of clearing form inputs.

## Quick start

- Modify your app by editing the `src/index.jsx` file.

- Build and deploy your app by running:

```
forge deploy
```

- Install your app in an Atlassian site by running:

```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:

```
forge tunnel
```

### Notes

- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.

Get in touch with me on **[@kipyegonline](https://www.twitter.com/kipyegonline)**
