import ForgeUI, {
  useState,
  useAction,
  useEffect,
  Text,
  TextArea,
  Fragment,
  Strong,
  Form,
  Checkbox,
  CheckboxGroup,
  DatePicker,
} from "@forge/ui";

function AddTask({ sendTask = (f) => f }) {
  const [task, setTask] = useState("");

  const handleSubmit = (formData) => {
    console.log("submit", formData);
    //setTask(formData.task);
    if (formData.task.trim()) {
      const newtask = { ...formData, id: "f-" + Date.now(), done: false };
      sendTask(newtask);
      setTask("");
    }

    console.log("submitir", task, formData);
  };
  return (
    <Fragment>
      <Form onSubmit={handleSubmit} submitButtonText="Add Task">
        <DatePicker name="date" label="Choose Date" />
        <TextArea
          label="Add Task"
          placeholder="Add task"
          name="task"
          defaultValue={task}
        />
      </Form>
    </Fragment>
  );
}
export default AddTask;

// cannot clear after submit
