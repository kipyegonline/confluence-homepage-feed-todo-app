// /* eslint-disable*/
import ForgeUI, {
  render,
  HomepageFeed,
  Fragment,
  useState,
  useEffect,
  Text,
  Avatar,
  useProductContext,
  Strong,
  DateLozenge,
} from "@forge/ui";
import { storage } from "@forge/api";
import AddTask from "./AddTasks";
import ShowTasks from "./showTasks";

export const App = () => {
  // get tasks from forge store if any
  const tasksApp = async () => await storage.get("My-tasks");

  const [tasks, setTasks] = useState(tasksApp() || []);

  const context = useProductContext();

  const deleteAll = () => {
    setTasks([]);
    storage.delete("My-tasks");
  };

  const getTask = (task) => {
    console.log("new task added");

    storage.set("My-tasks", [...tasks, task]);
    setTasks(tasksApp());
  };
  /*
  This hook is not working
  useEffect(() => {
    console.log("Changes in tasks", tasks);
  }, [tasks]);
*/
  console.log("The  FOrge App is running  onconfluence", tasks); // use Forge tunnel  to log these

  // jsx
  const summary = (
    <Fragment>
      <Text format="markup">
        <Strong>TODOS</Strong>
      </Text>

      <Avatar accountId={context.accountId} />
      <Text>
        {" "}
        <DateLozenge value={new Date().getTime()} />
      </Text>

      {/* ternary expression */}
      {tasks.length > 0 ? (
        tasks.length > 1 ? (
          <Text format="markup">
            <Strong>{tasks.length} tasks</Strong>
          </Text>
        ) : (
          <Text format="markup">
            <Strong>{tasks.length} task</Strong>
          </Text>
        )
      ) : null}

      {/* show task component if there are task(s) */}
      {tasks.length ? (
        <ShowTasks tasks={tasks} deleteAll={deleteAll} />
      ) : (
        <Text format="markup">
          <Strong> You have no tasks today </Strong>
        </Text>
      )}
      {/* Add task component */}
      <AddTask sendTask={getTask} />
    </Fragment>
  );
  return summary;
};

export const run = render(
  <HomepageFeed>
    <App />
  </HomepageFeed>
);
