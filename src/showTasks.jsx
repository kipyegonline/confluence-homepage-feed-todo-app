import ForgeUI, {
  useState,
  Text,
  Fragment,
  Strong,
  Toggle,
  Form,
  Button,
  CheckboxGroup,
  Checkbox,
} from "@forge/ui";
import { storage } from "@forge/api";

function ShowTasks({ tasks = [], deleteAll, sendChecked = (f) => f }) {
  const handleChange = (e) => {
    console.log(e, "Changes only");
    sendChecked(e);
  };
  const handleSubmit = (formData) => {
    storage.delete("My-tasks");
    deleteAll();
  };
  return (
    <Fragment>
      <Form onSubmit={handleSubmit} submitButtonText="Delete All">
        {tasks.map((task, index) => (
          <Fragment>
            <CheckboxGroup key={task.id} name={task.id} onChange={handleChange}>
              <Checkbox
                onChange={handleChange}
                onClick={handleChange}
                label={` ${index + 1}.   ${task.task} `}
              />
            </CheckboxGroup>
          </Fragment>
        ))}
      </Form>
    </Fragment>
  );
}

const CheckGrupList = () => (
  <Form onSubmit={handleSubmit} submitButtonText="Delete All">
    {tasks.map((task, index) => (
      <Fragment>
        <CheckboxGroup key={task.id} name={task.id} onChange={handleChange}>
          <Checkbox
            onChange={handleChange}
            onClick={handleChange}
            defaultChecked={index % 2 !== 0 ? true : false}
            value={task.done}
            label={` ${index + 1}.${task.task} `}
          />
        </CheckboxGroup>
      </Fragment>
    ))}
  </Form>
);
export default ShowTasks;

/*
            <Text>
              <CheckboxGroup name="completion">
                <Checkbox value="checked" label="jhfhfhfh" />
              </CheckboxGroup>
              {index + 1}.{task.task} {"   "}
</Text> */
